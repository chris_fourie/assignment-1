import java.lang.Math; // for Math.floor()
import java.util.concurrent.TimeUnit; // for timer

public class testing_Div_and_conq {

    public static void main(String[] args) {
        System.out.println("Divide and Conquer solution");

        //TODO make randomized array of between some two values -'ve and +'ve of incramental length through runs

        //int[] arr = {13, -3, -25, 20, -3, -16, -23, 18, 20, -7, 12, -5, -22, 15, -4, 7};
        int[] arr = {-13, -3, -25, -20, -3, -16, -23, -18, -20, -7, -12, -5, 22, 15, -4, 7};
        System.out.println("Initial array");
        for (int a = 0; a<arr.length; a++) System.out.print(arr[a] + " "); //print array

        //TODO add for loop to get average time over multiple runs and output to file in csv format for plotting
        //TODO add count to illustrate / graph step against time as well??
        //start timer
        long startTime = System.nanoTime();
        //Find max subarray
        Result r = findMaxSubArr_DAC(arr, 0, arr.length-1); //textbook uses 1 indexing and java uses 0 indexing for arrays
//        int mid_i = (int) Math.floor((double)((arr.length+1)/2));
//        Result r = findMaxCrossSubArr(arr, 0, mid_i, arr.length);

        //end timer
        long endTime = System.nanoTime();
        long timeElapsed = endTime - startTime;

        //print results
        System.out.println("\nlow_i: "+ r.first + " high_i: " + r.second);
        System.out.println("Max value of sub-array: "+ r.third);
        System.out.println("Max sub-array: ");
        for (int i = r.first; i <= r.second; i++) System.out.print(arr[i] + " ");
        System.out.println("\n \nTime elapsed: " + timeElapsed/1000 + " microseconds");
    }




    public static Result findMaxCrossSubArr (int[] arr, int low_i, int mid_i, int high_i){
        //Left
        double maxLeftSum = Double.NEGATIVE_INFINITY;
        int sum = 0;
        int maxL_i = mid_i;

        for (int i = mid_i; i >= low_i ; i--) {
            sum += arr[i];
            if (sum > maxLeftSum){
                maxLeftSum = sum;
                maxL_i  = i ;
            }
        }
        //Right
        double maxRightSum = Double.NEGATIVE_INFINITY;
        int maxR_i = mid_i + 1;
        sum = 0;
        for (int i = mid_i + 1; i < high_i ; i++) {
            sum += arr[i];
            if (sum > maxRightSum){
                maxRightSum = sum;
                maxR_i  = i ;
            }
        }
        return new Result (maxL_i, maxR_i, (int)(maxLeftSum + maxRightSum));
    }

    public static Result findMaxSubArr_DAC(int[] arr, int low_i, int high_i)
    {
        Result r_L = new Result(0,0,0); //first = leftLow_i, second = leftMid_i, third = maxLeftSum
        Result r_R = new Result(0,0,0);
        Result r_M = new Result(0,0,0);

        //base case - exit here
        if (high_i == low_i) {
            System.out.println(" --low_i " + low_i);
            return new Result (low_i,high_i,arr[low_i]);
        }
        //branches
        else {
            int mid_i = (int) Math.floor((double)((low_i+high_i)/2));
            System.out.println("Hi");
            r_L = findMaxSubArr_DAC(arr,low_i,mid_i);
            System.out.println("r_L");
            r_R = findMaxSubArr_DAC(arr,mid_i+1,high_i);
            System.out.println("r_R");
            r_M = findMaxCrossSubArr (arr, low_i, mid_i, high_i);
            System.out.println("r_M");
        }
        //terminal nodes
        if (r_L.third >= r_R.third && r_L.third >= r_M.third )  {
            System.out.println("left");
            return new Result (r_L.first,r_L.second,r_L.third);
        }
        else if (r_R.third >= r_L.third && r_R.third >= r_M.third ) {
            System.out.println("right");
            return new Result(r_R.first,r_R.second,r_R.third);
        }
        else {
            System.out.println("mid");
            return new Result(r_M.first, r_M.second, r_M.third);
        }
    }


    public static int sumSubArr (int i, int j,int[] arr) {
        int sum = 0;
        while (i <= j) {
            sum += arr[i];
            i++;
        }
        return sum ;
    }


    static final class Result {
        private final int first;
        private final int second;
        private final int third;

        public Result(int first, int second, int third) {
            this.first = first;
            this.second = second;
            this.third = third;
        }
    }

}

